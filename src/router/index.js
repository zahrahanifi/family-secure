import { createRouter, createWebHistory} from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'IndexPage',
    component: () => import('../views/Index'),
    meta: {
      title: 'خانه'
    }
  },
  {
    path: '/pages/service',
    name: 'ServicePage',
    component: () => import('../views/Service'),
    meta: {
      title: 'خدمات'
    }
  },
  {
    path: '/about-us',
    name: 'AboutPage',
    component: () => import('../views/About'),
    meta: {
      title: 'درباره ما'
    }
  },
  {
    path: '/contact-us',
    name: 'ContactPage',
    component: () => import('../views/Contact'),
    meta: {
      title: 'تماس با ما'
    }
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `لندینگ | ${to.meta.title}`
  next()
})

export default router
